package util;

import gui.StatusLabel;

public class XLException extends RuntimeException {
    
	private String message;
	
	public XLException(String message) {
        super(message);
        this.message = message;
        //setText i statuslabel
        //sl.setText(message);
    }
    
    public String getError(){
    	return message;
    }
}
package gui;

public class XLCounter {
    private int counter;
    //hur många xl som är öppna?

    public void increment() {
        counter++;
    }

    public String toString() {
        return Integer.toString(counter);
    }
}

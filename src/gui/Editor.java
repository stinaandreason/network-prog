package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import javafx.beans.InvalidationListener;
import modell.Slot;
//import javafx.beans.Observable;
import modell.SpreadSheet;
import util.XLException;

//textfält för att skriva in värden

public class Editor extends JTextField implements ActionListener, Observer {
	private CurrentSlot cs;
	private SpreadSheet ss;
	private StatusLabel sl;


	public Editor(CurrentSlot cs, SpreadSheet ss, StatusLabel sl) {

		this.cs = cs;
		this.ss = ss;
		this.sl = sl;
		cs.addObserver(this);
		setBackground(Color.WHITE);
		this.addKeyListener(new KeyListener() {

			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) { // läser av när man
															// trycker på enter
					if (!read().isEmpty()) {

						System.out.println("read:" + read());
						String text = read();
						String adress = cs.getSlotCode();
						System.out.println("Adress: " + cs.getSlotCode());

						try {
							ss.put(adress, text);
						} catch (XLException e1) {
							sl.setText(e1.getMessage());
							System.out.println(e1.getMessage());
						} catch (IOException e1) {
							sl.setText(e1.getMessage());
						}

					}

				}

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	public String read() {
		return this.getText();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		String adress = cs.getSlotCode();
		if (!cs.isEmpty()) {
			String stringSlot = ss.getSlotString(adress);
			setText(stringSlot);
		}

	}

}
package gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.SwingConstants;

import modell.SpreadSheet;

//skriver ut kolnr

public class SlotLabels extends GridPanel implements Observer {
	private List<SlotLabel> labelList;
	private CurrentLabel cl;
	private CurrentSlot cs;

	private SpreadSheet ss;
	private StatusLabel sl;
	private int saveInt;
	private SlotLabel current;

	public SlotLabels(int rows, int cols, CurrentSlot cs, SpreadSheet ss, StatusLabel sl) {
		super(rows + 1, cols);
		this.ss = ss;
		this.sl = sl;
		ss.addObserver(this);
		labelList = new ArrayList<SlotLabel>(rows * cols);
		for (char ch = 'A'; ch < 'A' + cols; ch++) {
			add(new ColoredLabel(Character.toString(ch), Color.LIGHT_GRAY, SwingConstants.CENTER));
		}
		for (int row = 1; row <= rows; row++) {
			for (char ch = 'A'; ch < 'A' + cols; ch++) {
				String name = ch + "" + row;
				SlotLabel label = new SlotLabel(name, cs, ss, sl);
				label.addMouseListener(new SlotListener(cs, label));
				add(label);
				labelList.add(label);
			}
		}

	}

	public int saveInt(int slot) {
		saveInt = slot;
		return saveInt;
	}

	@Override
	public void update(Observable o, Object arg) {
//		// skriver ut i rutan, vill skriva ut value		
		for (SlotLabel sl : labelList) {
			sl.setText(ss.print(sl.getName()));
		}


	}

	public void setAllWhite() {
		for (int i = 0; i < 80; i++) {
			labelList.get(i).setBackground(Color.WHITE);

		}

	}

	public void loopSlots() {

	}

}

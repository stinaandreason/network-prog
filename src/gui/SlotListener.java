package gui;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
public class SlotListener implements MouseListener {

	private CurrentSlot cs;
	private SlotLabel sl;
	public SlotListener(CurrentSlot cs, SlotLabel sl) {
		this.cs = cs;
		this.sl = sl;
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		cs.setCurrent(sl);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}

package gui;

import java.awt.Color;
import java.util.Observable;


//ska läsa av vilken ruta man är i

public class CurrentSlot extends Observable {
	private SlotLabel slot;
	private CurrentLabel cl;
	private CurrentSlot cs;
	private String address;

	
	public CurrentSlot(){
		
	}
	
	public String getSlotCode(){
		return slot.getName();
	
		
	}
	
	public void setCurrent(SlotLabel s){
		slot = s;		
		addObserver(s);
		setChanged();
		notifyObservers();
		
	}
	
	public SlotLabel getCurrent() {
		return slot;
	}
	
	public void clear(){
		slot.setText("");
	}
	
	public void setWhiteBackground(){
		
		slot.setBackground(Color.WHITE);
	}




	public void setText(String print) {
		// TODO Auto-generated method stub
		slot.setText(print);
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(slot != null){
			return false;
		} else {
			return true;
		}
	}


	public void loopSlots(){
		notifyObservers();
	}


}
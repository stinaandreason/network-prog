package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

import gui.XL;

class ClearAllMenuItem extends JMenuItem implements ActionListener {
    
	private XL xl;
	
	public ClearAllMenuItem(XL xl) {
        super("Clear all");
        addActionListener(this);
        this.xl = xl;
        
    }

    public void actionPerformed(ActionEvent e) {
        // TODO
    	//gå igenom hashmap och töm värden + uppdatera view
    	xl.getSpreadSheet().clearAll();
    	//notifyObservers();
    	
    }
}
package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

import gui.CurrentSlot;
import gui.XL;

class ClearMenuItem extends JMenuItem implements ActionListener {
	
	private XL xl;
	private CurrentSlot cs;
	
    public ClearMenuItem(XL xl, CurrentSlot cs) {
        super("Clear");
        addActionListener(this);
        this.xl = xl;
        this.cs = cs;
    }

    public void actionPerformed(ActionEvent e) {
        // TODO
    	//ta bort innehållet i en ruta?? 
    	xl.getSpreadSheet().remove(cs.getSlotCode());
    }
}
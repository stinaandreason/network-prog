package gui.menu;

import gui.StatusLabel;
import gui.XL;
import modell.SpreadSheet;

import java.io.FileNotFoundException;
import javax.swing.JFileChooser;

class LoadMenuItem extends OpenMenuItem {
	SpreadSheet s;
    public LoadMenuItem(XL xl, StatusLabel statusLabel, SpreadSheet s) {
        super(xl, statusLabel, "Load");
        this.s = s;
    }

    protected void action(String path) throws FileNotFoundException {
        s.load(path);
    }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showOpenDialog(xl);
    }
}
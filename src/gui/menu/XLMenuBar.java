package gui.menu;

import gui.CurrentSlot;
import gui.StatusLabel;
import gui.XL;
import gui.XLList;
import modell.SpreadSheet;

import javax.swing.JMenu;
import javax.swing.JMenuBar;

public class XLMenuBar extends JMenuBar {
	public XLMenuBar(XL xl, XLList xlList, StatusLabel statusLabel, CurrentSlot cs, SpreadSheet s) {
		JMenu file = new JMenu("File");
		JMenu edit = new JMenu("Edit");
		file.add(new PrintMenuItem(xl, statusLabel));
		file.add(new SaveMenuItem(xl, statusLabel, s));
		file.add(new LoadMenuItem(xl, statusLabel, s));
		file.add(new NewMenuItem(xl));
		file.add(new CloseMenuItem(xl, xlList));
		edit.add(new ClearMenuItem(xl, cs));
		edit.add(new ClearAllMenuItem(xl));
		add(file);
		add(edit);
		add(new WindowMenu(xlList));
	}
}
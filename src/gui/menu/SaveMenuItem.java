package gui.menu;

import gui.StatusLabel;
import gui.XL;
import modell.SpreadSheet;

import java.io.FileNotFoundException;
import javax.swing.JFileChooser;

class SaveMenuItem extends OpenMenuItem {
	SpreadSheet s;
	
    public SaveMenuItem(XL xl, StatusLabel statusLabel, SpreadSheet s) {
        super(xl, statusLabel, "Save");
        this.s = s;
    }

    protected void action(String path) throws FileNotFoundException {
        s.save(path);
    }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showSaveDialog(xl);
    }
}
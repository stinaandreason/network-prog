package gui;

import java.awt.Color;

//skriver ut errors etc 

import java.util.Observable;
import java.util.Observer;

import util.XLException;

public class StatusLabel extends ColoredLabel implements Observer {
	
	private CurrentSlot cs;
	private XLException xlex;
	
    public StatusLabel(CurrentSlot cs) {
        super("", Color.WHITE);
        this.cs = cs;
        cs.addObserver(this); //ss.addObserver(this); vilken?
    }

    public void update(Observable observable, Object object) {
        if(xlex != null){
        	setText(xlex.getError());
        } 
    }
    
    public String getText(){
    	return "";
    }
}
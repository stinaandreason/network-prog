package gui;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import modell.SpreadSheet;

//alla rutor

public class SlotLabel extends ColoredLabel implements Observer {
	
	private String name;
	private CurrentSlot cs;
	private SpreadSheet ss;
	private StatusLabel sl;
	
	public SlotLabel(String name, CurrentSlot cs, SpreadSheet ss,
			StatusLabel sl) {
        super("                    ", Color.WHITE, RIGHT);
        this.name = name;
        this.cs = cs;
        this.ss = ss;
        this.sl = sl;
     
    }

	@Override
	public void update(Observable o, Object arg) {
		if (cs.getCurrent() == this) {
			setBackground(Color.YELLOW);
		} else {
			setBackground(Color.WHITE);
			cs.deleteObserver(this);
			
		}
	}
	
    @Override
	public String getName(){
		return name;
    	
	}
    
    public void setCurrent(){
    	
    }

	public int getNumber() {
		// TODO Auto-generated method stub
		return 0;
	}

}
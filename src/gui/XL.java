package gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.NORTH;
import static java.awt.BorderLayout.SOUTH;
import gui.menu.XLMenuBar;
import modell.SpreadSheet;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class XL extends JFrame implements Printable {
    private static final int ROWS = 10, COLUMNS = 8;
    private XLCounter counter;
    
    
    
    private XLList xlList;
    private SlotLabels slotLabels;
    private SpreadSheet ss = new SpreadSheet();
    
    private CurrentSlot cs = new CurrentSlot(); //currentslot vill ha sheetpanel och sheetpanel vill ha currentslot? 
    private StatusLabel statusLabel = new StatusLabel(cs);
    private SheetPanel sp;
    private CurrentLabel cl = new CurrentLabel(cs);
    

    public XL(XL oldXL) {
        this(oldXL.xlList, oldXL.counter);
    }

    public XL(XLList xlList, XLCounter counter) {
    	
    	
    	
        super("Untitled-" + counter);
        slotLabels = new SlotLabels(ROWS, COLUMNS, cs, ss, statusLabel);
        this.xlList = xlList;
        
        this.counter = counter;
        xlList.add(this);
        counter.increment();

        JPanel statusPanel = new StatusPanel(statusLabel, cs);
        JPanel sheetPanel = new SheetPanel(ROWS, COLUMNS, cs, ss, statusLabel);
        Editor editor = new Editor(cs, ss, statusLabel);

        
        add(NORTH, statusPanel);
        add(CENTER, editor);
        add(SOUTH, sheetPanel);
        setJMenuBar(new XLMenuBar(this, xlList, statusLabel, cs, ss));
        pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
    }

    public int print(Graphics g, PageFormat pageFormat, int page) {
        if (page > 0)
            return NO_SUCH_PAGE;
        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        printAll(g2d);
        return PAGE_EXISTS;
    }

    public void rename(String title) {
        setTitle(title);
        xlList.setChanged();
    }

    public static void main(String[] args) {
        new XL(new XLList(), new XLCounter());
    }

	public SpreadSheet getSpreadSheet() {
		// TODO Auto-generated method stub
		return ss;
	}
}
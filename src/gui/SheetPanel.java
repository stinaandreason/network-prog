package gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.WEST;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observer;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import modell.SpreadSheet;

//rutnätet inkl radnr och kolnr

public class SheetPanel extends BorderPanel implements Observer  { //observer

	private SlotLabels sl;
	public SheetPanel(int rows, int columns, CurrentSlot cs, SpreadSheet ss, StatusLabel stl) {
		add(WEST, new RowLabels(rows));
		sl = new SlotLabels(rows, columns, cs, ss, stl);
		add(CENTER, sl);		
		ss.addObserver(this);

	}
	
	@Override
	public void update(java.util.Observable o, Object arg) { //varför måste det stå java.util??
		// TODO Auto-generated method stub
		
		
	}


}
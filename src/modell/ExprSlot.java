package modell;
import expr.Environment;
import expr.Expr;

public class ExprSlot implements Slot {
	private Expr expr;
	private String s;

	public ExprSlot(String s, Expr expr) {
		this.s = s;
		this.expr = expr;
	}
	
	@Override
	public String toString(){
		return expr.toString();
	}
	
	@Override
	public double value(Environment e){
		return expr.value(e);
	}
	
}

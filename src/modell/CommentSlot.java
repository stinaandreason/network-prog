package modell;
import expr.Environment;

public class CommentSlot implements Slot {
	private String comment;
	private String address;

	public CommentSlot(String address, String comment) {
		this.comment = comment;
		this.address = address;
	}
	
	@Override
	public String toString(){
		return comment;
	}

	@Override
	public double value(Environment e) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String getCommentValue(){
		return comment;
	}

}

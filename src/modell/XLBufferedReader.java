package modell;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;

import gui.StatusLabel;
import util.XLException;

//TODO move to another package
public class XLBufferedReader extends BufferedReader {
	private SpreadSheet s;
	private StatusLabel sl;
	
    public XLBufferedReader(String name) throws FileNotFoundException {
        super(new FileReader(name));
        s = new SpreadSheet();
       
    }

    // TODO Change Object to something appropriate
    public void load(Map<String, Slot> map) {
        try {
            while (ready()) {
                String string = readLine();
                int i = string.indexOf('=');
                String address = string.substring(0, i);
                String slot = string.substring(i + 1);
                map.put(address, s.buildSlot(address, slot));
            }
        } catch (Exception e) {
            throw new XLException(e.getMessage());
        }
    }
}

package modell;
import expr.Environment;
import util.XLException;

public class BombSlot implements Slot {
	private String command;
	
	public BombSlot(String command) {
		this.command = command;
	}
	
	@Override
	public String toString(){
		return command;
	}

	@Override
	public double value(Environment e) {
		// TODO Auto-generated method stub
		throw new XLException("Bombslot");
	}
	
	/* throw new XLException circular metod */
}

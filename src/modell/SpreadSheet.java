package modell;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Observable;

import expr.Environment;
import expr.Expr;
import expr.ExprParser;
import gui.SlotLabels;
import gui.StatusLabel;
import util.XLException;

public class SpreadSheet extends Observable implements Environment {
	HashMap<String, Slot> map;
	private ExprParser exprParser;

	public SpreadSheet() {
		map = new HashMap<String, Slot>();
		exprParser = new ExprParser();
		
	}

	@Override
	public double value(String address) {
		Slot s = map.get(address);
		if (s == null) {
			throw new XLException("Empty slot");
			
		}
		notifyObservers(); 
		
		/* gör om slot-strängen till double/value */
		return s.value(this);
	}

	/* put metod som ska anropas när man läser av rutorna */
	public void put(String address, String s) throws IOException {
		
		Slot previousSlot = map.get(address);
		Slot newSlot = buildSlot(address, s);
		map.put(address, newSlot);
		try {
		checkBomb(address, newSlot);
		} catch (XLException e) {
			if(previousSlot == null){
				remove(address);
			} else {
				map.put(address, previousSlot);
			}
			throw new XLException(e.getMessage());
		}
		setChanged();
		notifyObservers(); //uppdatera hela rutnätet
	}

	public Slot buildSlot(String address, String s) { //gör map.put direkt här!
		if (s.startsWith("#")) {
			Slot cSlot = new CommentSlot(address, s);
			setChanged();
			notifyObservers();
			return cSlot;
		} else {
			try {
				Expr ex = exprParser.build(s);
				Slot eSlot = new ExprSlot(address, ex); 
				setChanged();
				notifyObservers();
				return eSlot;
			} catch (Exception e) {
				
				throw new XLException("Invalid");
				
			}
		}
		
	}
	
	private void checkBomb(String address, Slot slot){
		Slot previousSlot = map.get(address);
		BombSlot bSlot = new BombSlot("");
		map.put(address, bSlot);
		try {
			slot.value(this);
		} finally {
			map.put(address, previousSlot);
		}
	}
	
	public void save(String file) throws FileNotFoundException{
		XLPrintStream printer = new XLPrintStream(file);
		printer.save(map.entrySet());
	}
	
	public void load(String file) throws FileNotFoundException{
		clearAll();
		XLBufferedReader reader = new XLBufferedReader(file);
		reader.load(map);
		setChanged();
		notifyObservers();
	}

	public void clearAll() {
		map.clear();
		setChanged();
		notifyObservers();
	}
	
	public void remove(String address){
		map.remove(address);
		setChanged();
		notifyObservers();
	}

	public void addSlotLabels(SlotLabels sl) {
		// TODO Auto-generated method stub
		
	}

	public String getSlotString(String adress) {
		// TODO Auto-generated method stub
			if(map.get(adress) != null){
				return map.get(adress).toString();
			} else {
				return "";
			}
	}

	public boolean isEmpty(String address) {
		// TODO Auto-generated method stub
		if(map.get(address) == null) {
			return false;
		} else {
			return true;
		}
	}

	public String print(String address) {
		// TODO Auto-generated method stub
		Slot s = map.get(address);
		if(s==null){
			return "";
		} else if (s instanceof CommentSlot) {
			return ((CommentSlot) s).getCommentValue();			
		} else {
			return "" + s.value(this); //this ist för environment
			
		}
		
	}

}

<<<<<<< HEAD
# network prog

=======
Användarfall:
Change value - användaren ändrar värdet i en ruta genom att markera rutan och skriva in ett värde. Det blir problem om vi t.ex. sparar “b1” i “b2” eller skriver “a1+1” i a1.

Add - användaren adderar 2 tal genom att ta innehållet i två rutor och sen skriva plus mellan dem, t.ex. A1 + A2 och sen spara det i den tredje rutan där användaren skrivit in “A1 + A2”. Problemen som kan uppstå är t.ex. om det inte är en int/double mm inlagd så att den inte kan addera värdena. 

Multiply - användaren multiplicerar 2 tal genom att ta innehållet i två rutor och sen skriva gånger mellan dem, t.ex. A1 * A2 och sen spara det i en tredje ruta. Problemen som kan uppstår är t.ex. om det inte är en int/double mm inlagd så att den inte kan multiplicera värdena. 

Divide - användaren dividerar 2 tal genom att ta innehållet i två rutor och sen skriva delat med mellan dem, t.ex. A1 / A2 och sen spara det i en tredje ruta. Problemen som kan uppstår är t.ex. om det inte är en int/double mm inlagd så att den inte kan dividera värdena. Det blir även problem om användaren försöker dividera med noll.

Subtract - användaren subtraherar 2 tal genom att ta innehållet i två rutor och sen skriva minus mellan dem, t.ex. A1 - A2 och sen spara det i en tredje ruta. Problemen som kan uppstår är t.ex. om det inte är en int/double mm inlagd så att den inte kan subtrahera värdena. 

Close - användaren stänger ned programmet genom att välja close i menyraden. Inget sparas och programmer stängs ned.

Save - användaren sparar en fil av det öppna kalkylbladet genom att välja save i menyn. Filen sparas då i ett dokument. Det kan bli problem om användaren skriver in ett felaktigt filnamn eller försöker spara filen på ett ställe som inte finns.

Open - användaren öppnar en fil från tidigare genom att välja open i menyn. Allt i det öppna kalkylfönstret byts ut till innehållet av den öppnade filen. Kan bli problem om användaren öppnar en fil i fel format eller en fil som inte existerar.

New - användaren skapar ett nytt kalkylblad genom att välja new i menyn. Filen får namnet “untitled-x” där x är numret+1 på en redan sparad fil med samma namn.

Print - användaren skriver ut innehållet i filen genom att välja print i menyn. All data i kalkylbladet samts dess form läses då av och skrivs ut. Kan bli problem om man inte har en skrivare ansluten.

Vilka klasser bör finnas för att representera ett kalkylark?
Klasser: Worksheet, slot

En ruta i kalkylarket skall kunna innehålla en text eller ett uttryck. Hur modellerar man detta?
Template pattern, där tex “innehåll” (Environment?) är ett interface som implementeras av text och expression.

Hur skall man hantera uppdragsgivarens krav på minnesresurser?
    “Skapa” en ny ruta varje gång man gör ett nytt expression.

Vilka klasser skall vara observatörer och vilka skall observeras?
Observer: StatusLabel, CurrentSlot, WindowMenu
Observable: XLList, Slot (både innehåll och adress)

Vilket paket och vilken klass skall hålla reda på vad som är "Current slot"?
    Gui - CurrentSlot

Vilken funktionalitet är redan färdig och hur fungerar den? Titta på klasserna i view-paketet och testkör.
Man får upp ett kalkylark där man kan skriva i inputboxen men man kan inte markera några rutor. Menyknapparna funkar också. Edit är svårt att se eftersom det inte finns nåt att cleara, men window och file funkar.

Det kan inträffa ett antal olika fel när man försöker ändra innehållet i ett kalkylark. Då skall undantag kastas. Var skall dessa undantag fångas och hanteras?
    XLException hanterar undantagen.

Vilken klass används för att representera en adress i ett uttryck?
    Slot har en adress med kolumn och rad - ny Adress-klass?
    
När ett uttryck som består av en adress skall beräknas används gränssnittet Environment. Vilken klass skall implementera gränssnittet? 
Varför använder man inte klassnamnet i stället för gränssnittet?
    Expression

Om ett uttryck i kalkylarket refererar till sig själv, direkt eller indirekt, så kommer det att bli bekymmer vid beräkningen av uttryckets värde. Föreslå något sätt att upptäcka sådana cirkulära beroenden! Det finns en elegant lösning med hjälp av strategimönstret som du får chansen att upptäcka. Om du inte hittar den så kommer handledaren att avslöja den.
    
>>>>>>> OMD inl2
